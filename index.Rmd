---
title: "Easily publish an R bookdown website with GitLab Pages"
author: "Romain Lesur"
description: "This book provides a demo of a bookdown website published on GitLab Pages"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
bibliography: packages.bib
biblio-style: apalike
link-citations: yes
favicon: "favicon.ico"
output:
  bookdown::gitbook: 
    includes:
      after_body: disqus.html
    config:
      toc:
        after: |
          <li><a href="https://gitlab.com/RLesur/bookdown-gitlab-pages">
          See the source</a></li>
      edit: https://gitlab.com/RLesur/bookdown-gitlab-pages/edit/master/%s
      download: "pdf"
  bookdown::pdf_book: 
    latex_engine: pdflatex
    citation_package: natbib
---

# Overview: a simple workflow {-}
```{r setup, include=FALSE}
library(magrittr)
```

```{r write-bib, include=FALSE}
knitr::write_bib(c(.packages(), 'bookdown'), 'packages.bib')
```

Publishing an `R` [@R-base] `bookdown` [@R-bookdown] website with [`GitLab Pages`](https://about.gitlab.com/features/pages/) is as easy as:

- hosting a repository on [`GitLab`](https://gitlab.com)
- adding a configuration file to the project

## Host a project on <code>GitLab</code> {-}

As [`GitHub`](https://www.github.com), [`GitLab`](https://gitlab.com) is a web-based [`Git`](https://git-scm.com/) repository manager. Creating a new project on [`GitLab`](https://gitlab.com) is fairly intuitive for [`GitHub`](https://www.github.com) users. [`GitLab`](https://gitlab.com) users can create unlimitate private projects for free (see [here](https://about.gitlab.com/gitlab-com/)). 

[`GitLab`](https://gitlab.com) also offers a continuous integration service ([`GitLab CI`](https://about.gitlab.com/features/gitlab-ci-cd/)) and a static websites hosting service ([`GitLab Pages`](https://about.gitlab.com/features/pages/)) in its free plan ^[CI pipelines are limited to 2,000 minutes per month].  
These features are also present in the open source software [`GitLab Community Edition (CE)`](https://about.gitlab.com/products/).

## Add a `GitLab CI` configuration file {-}

Add the following `.gitlab-ci.yml` file in the root of the project:
```{r insert-ci-file, comment=NA, class.output='yaml', echo=FALSE}
cat(readChar("./.gitlab-ci.yml", 1e5))
```

For explanations, see section \@ref(CI-file-details).

## See the result {-}

When the `CI` job is done, the website is served on `GitLab Pages`.  
CI jobs status can be found in `CI / CD menu > Jobs`. An example, [here](https://gitlab.com/RLesur/bookdown-gitlab-pages/-/jobs).  
The address of the `GitLab Pages` project can be found in `Settings > Pages`. An example, [here](https://gitlab.com/RLesur/bookdown-gitlab-pages/pages).


