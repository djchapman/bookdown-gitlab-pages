# DJ's published R bookdown website with GitLab Pages

This repository is a bookdown demo website served on `GitLab Pages` using `GitLab CI`.

See https://rlesur.gitlab.io/bookdown-gitlab-pages
