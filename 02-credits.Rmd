# Credits

Combining `bookdown` and `GitLab` with `Docker` leads to a very simple workflow.

Many thanks to:

- [Yihui Xie](https://yihui.name/) for `bookdown`
- [Dirk Eddelbuettel](http://dirk.eddelbuettel.com/) and [Carl Boettiger](http://carlboettiger.info/) for `Rocker`
- [`GitLab` Core Team](https://about.gitlab.com/core-team/) for `GitLab CE`
- `Docker CE` authors
